# IT Project

### Background

IT Project is just another IT project management web-based application.  I created this for school back in 2009 using PHP and MySQL.  Thought I'd resurrect this and use this as my "test subject" for learning more about the DevOps tools/technologies (i.e. Git, Docker, Kubernetes, etc), as well as other new technologies (i.e. NoSQL, etc).  This will also serve as a way of re-acquainting myself with some of the tools I have used before (i.e. MySQL, PHP, etc).


Most of my professional experience have been in the IT infrastructure side of things.  The last five years of my professional life has been spent on the physical (and some virtual) side of IT infrastructure dealing mostly with Open source software and Oracle technologies (i.e. Weblogic, Oracle database, etc).   During those five years, while I did used a bit of the newer technologies (i.e. used Ansible to automate a recurring deployment, and got to work a bit on Oracle Cloud Infrastructure), that was it - so I have a lot of catching up to do :-)


### Installing / Deploying

I've created a couple of [Dockerfiles of this application](https://github.com/jsanjuanph/docker) as well as [Docker images](https://cloud.docker.com/repository/docker/jsanjuansg/itproject-apache2)


## Built With

* [PHP](https://php.net/) 
* [MySQL / MariaDB](https://mariadb.org/) 

* [Redis](https://redis.io/)
    * 06/13/2019: I've created a new branch [mysql-redis](https://bitbucket.org/jsanjuansg/it-project/branch/mysql-redis) where I added code using [phpredis](https://github.com/phpredis/phpredis).  Initially using Redis for a hit counter :-)

* [PostgreSQL](https://www.postgresql.org/)
    * 06/14/2019: I've created this version of [IT Project using PostgreSQL](https://gitlab.com/jsanjuansg/it-project-pgsql) as the database backend.

## Author

* **Joshua San Juan** - *Initial work* 


## License

This project is licensed under the GNU General Public License v3.0


## Acknowledgments

* Thanks to all people who took the time to post helpful content on the Internet.
* Copied a bit of the README.md from this template - https://gist.github.com/PurpleBooth/109311bb0361f32d87a2

