-- MySQL dump 10.14  Distrib 5.5.60-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: itproject
-- ------------------------------------------------------
-- Server version	5.5.60-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `itproject`
--

/*!40000 DROP DATABASE IF EXISTS `itproject`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `itproject` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `itproject`;

--
-- Table structure for table `announcements`
--

DROP TABLE IF EXISTS `announcements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `username` varchar(45) NOT NULL,
  `text` longtext NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `privacy` int(11) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `announcements`
--

LOCK TABLES `announcements` WRITE;
/*!40000 ALTER TABLE `announcements` DISABLE KEYS */;
INSERT INTO `announcements` VALUES (13,'2010-07-09 10:13:51','jinky','We will have a general assembly meeting at the 33rd floor',0,1,'General Assembly');
/*!40000 ALTER TABLE `announcements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar`
--

DROP TABLE IF EXISTS `calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `username` varchar(45) NOT NULL,
  `project_id` int(11) NOT NULL,
  `text` varchar(120) NOT NULL,
  `privacy` int(11) NOT NULL COMMENT '1=private\n0=public',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar`
--

LOCK TABLES `calendar` WRITE;
/*!40000 ALTER TABLE `calendar` DISABLE KEYS */;
INSERT INTO `calendar` VALUES (20,'2010-07-20 23:30:00','jsanjuan',0,'test 11:30pm',1);
/*!40000 ALTER TABLE `calendar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat` (
  `id` varchar(11) NOT NULL,
  `fromuser` varchar(45) DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  `message` varchar(400) DEFAULT NULL,
  `touser` varchar(45) DEFAULT NULL,
  `cread` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat`
--

LOCK TABLES `chat` WRITE;
/*!40000 ALTER TABLE `chat` DISABLE KEYS */;
/*!40000 ALTER TABLE `chat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `username` varchar(45) NOT NULL,
  `text` longtext NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `privacy` int(11) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `name` varchar(200) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `fdate` datetime DEFAULT NULL,
  `privacy` int(11) NOT NULL COMMENT '1=private\n0=public',
  `description` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES ('sorting.doc','jinky',1,'2010-07-09 10:00:38',1,'test document');
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forums`
--

DROP TABLE IF EXISTS `forums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projects_id` int(11) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `body` longtext NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `privacy` int(2) DEFAULT NULL,
  `author` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forums`
--

LOCK TABLES `forums` WRITE;
/*!40000 ALTER TABLE `forums` DISABLE KEYS */;
INSERT INTO `forums` VALUES (7,0,'This is the first Topic','Topic Body','2010-07-05 00:00:00',1,'jsanjuan'),(8,0,'this is the second topic','testing of the 2nd topic','2010-07-06 00:00:00',1,'jsanjuan');
/*!40000 ALTER TABLE `forums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forums_messages`
--

DROP TABLE IF EXISTS `forums_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forums_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projects_id` int(11) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `body` longtext NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `privacy` int(2) DEFAULT NULL,
  `author` varchar(120) NOT NULL,
  `forums_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forums_messages`
--

LOCK TABLES `forums_messages` WRITE;
/*!40000 ALTER TABLE `forums_messages` DISABLE KEYS */;
INSERT INTO `forums_messages` VALUES (4,0,'','this is a response to 1st','2010-07-05 00:00:00',1,'jsanjuan',7),(5,0,'','another response','2010-07-05 00:00:00',1,'jsanjuan',7);
/*!40000 ALTER TABLE `forums_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group`
--

DROP TABLE IF EXISTS `group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `desc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group`
--

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` VALUES (1,'default','default group'),(2,'Office Administration','Office Administration'),(3,'HR','HR'),(4,'Sales','Sales'),(5,'Software Development','Software Development'),(6,'Network','Network'),(7,'Systems','Systems'),(8,'DevOps','DevOps'),(9,'Project Management','Project Management');
/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `username` varchar(45) NOT NULL,
  `text` longtext NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `privacy` int(11) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (19,'2010-07-26 14:48:09','jinky','test',0,1,'test');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `position`
--

DROP TABLE IF EXISTS `position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `desc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `position`
--

LOCK TABLES `position` WRITE;
/*!40000 ALTER TABLE `position` DISABLE KEYS */;
INSERT INTO `position` VALUES (1,0,'Administrator','Admin'),(2,5,'Java Developer','Java Developer'),(3,5,'PHP Developer','PHP Developer'),(4,5,'Golang Developer','Golang Developer'),(5,5,'Python Developer','Python Developer'),(6,5,'C++ Developer','C++ Developer'),(7,7,'Windows Systems Administrator','Windows Systems Administrator'),(8,7,'AIX Systems Administrator','AIX Systems Administrator'),(9,7,'Solaris Systems Administrator','Solaris Systems Administrator'),(10,7,'Linux Systems Administrator','Linux Systems Administrator'),(11,9,'Account Manager 1','Account Manager 1'),(12,9,'Account Manager 2','Account Manager 2'),(13,9,'Account Manager 3','Account Manager 3'),(16,9,'Senior Account Manager 1','Senior Account Manager 1'),(17,9,'Senior Account Manager 2','Senior Account Manager 2'),(18,9,'Senior Account Manager 3','Senior Account Manager 3'),(19,9,'Project Manager 1','Project Manager 1'),(20,9,'Project Manager 2','Project Manager 2'),(21,9,'Project Manager 3','Project Manager 3'),(22,3,'Recruiter 1','Recruiter 1'),(23,3,'Recruiter 2','Recruiter 2'),(24,3,'Recruiter 3','Recruiter 3'),(25,3,'Recruiter 4','Recruiter 4'),(26,3,'Recruiter 5','Recruiter 5'),(27,6,'Network Engineer 1','Network Engineer 1'),(28,6,'Network Engineer 2','Network Engineer 2'),(29,6,'Network Engineer 3','Network Engineer 3'),(30,6,'Network Engineer 4','Network Engineer 4'),(31,6,'Senior Network Engineer 1','Senior Network Engineer 1'),(32,6,'Senior Network Engineer 2','Senior Network Engineer 2'),(33,8,'DevOps 1','DevOps 1'),(34,8,'DevOps 2','DevOps 2'),(35,4,'Sales Executive 1','Sales Executive 1'),(36,4,'Sales Executive 2','Sales Executive 2'),(37,4,'Sales Executive 3','Sales Executive 3'),(38,4,'Senior Sales Executive 1','Senior Sales Executive 1'),(39,4,'Senior Sales Executive 2','Senior Sales Executive 2'),(40,3,'HR Executive 1','HR Executive 1'),(41,3,'HR Executive 2','HR Executive 2'),(42,3,'HR Manager','HR Manager'),(43,7,'Systems Manager','Systems Manager'),(44,6,'Network Manager','Network Manager'),(45,8,'DevOps Manager','DevOps Manager'),(46,4,'Sales Manager','Sales Manager'),(47,2,'Office Manager','Office Manager'),(48,2,'Office Assistant Manager','Office Assistant Manager'),(49,5,'Software Development Manager','Software Development Manager');
/*!40000 ALTER TABLE `position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) NOT NULL,
  `manager` varchar(45) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `actual_start_date` date DEFAULT NULL,
  `actual_end_date` date DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT 'done=1\nongoing=2\non hold=3\ncancelled=4\n',
  `privacy` int(11) NOT NULL COMMENT '0=private\n1=public',
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (53,'test','jsanjuan','2010-01-01','2010-08-04','2010-01-01','2010-08-04',2,1,'test'),(51,'Online Registration Project','jsanjuan','2010-01-01','2010-07-14','2010-01-01','2010-07-14',2,1,'Online registration project for a cinema');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_members`
--

DROP TABLE IF EXISTS `projects_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_members` (
  `project_id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_members`
--

LOCK TABLES `projects_members` WRITE;
/*!40000 ALTER TABLE `projects_members` DISABLE KEYS */;
INSERT INTO `projects_members` VALUES (49,'jsanjuan'),(52,'miket'),(51,'jinky'),(53,'jinky'),(54,'jsanjuan');
/*!40000 ALTER TABLE `projects_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `username` varchar(45) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `email` varchar(120) NOT NULL,
  `position_id` int(11) NOT NULL COMMENT '0 = staff ; 1 = manager',
  `group_id` int(11) NOT NULL,
  `password` varchar(12) NOT NULL,
  `online` int(11) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('administrator','Administrator','','admin@itproject-portal.com',1,0,'password',0),('jsanjuan','Joshua','San Juan','jsanjuan@test.com',0,0,'password',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-12  5:57:07
