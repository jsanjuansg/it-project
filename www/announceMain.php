<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: News Main</title>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain"><img src="img/announce.gif" width="32" height="32"> Announcements</td>
        </tr>
        <tr class="maintext">
          <td width="21%"><a href="announceCreateNew.php">Create New Announcement</a></td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
            <tr>
              <td width="44%" class="menubar">Date</td>
              <td width="20%" class="menubar">Subject</td>
            </tr>
            <tr>
            

<?php

$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}


/*

id
date
username
text
project_id
privacy
subject

*/

$sql = "SELECT id,date,subject FROM announcements";

//echo $sql;


$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $query;
    die($message);
}

while ( $row = $result->fetch_assoc() ) {
	echo "<tr>\n";
	
	echo "<td>\n";
	echo $row['date'];
	echo "</td>\n";
	
	echo "<td>\n";	
	$announce_url = "<a href=\"" . $itproject_url . "/";
	$announce_url .= "announceView.php?id=" . $row['id'];
	$announce_url .= "\">";
	echo $announce_url;
	echo $row['subject'];
	echo "</a>";
	echo "</td>\n";
	

//	status (1 = done; 2 = ongoing; 3 = onhold ; 4 = cancelled)


	echo "</tr>\n";
}

 
$conn->close();


?>

            
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </tr>
</table>
</body>
</html>
