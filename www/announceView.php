<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$announceid = trim($_GET['id']);


$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}

$username = $_SESSION['itp_username'];

/*

id
date
username
text
project_id
privacy ( 0 = private; 1 = public)
subject

mysql_insert_id 

*/
$sql = sprintf("SELECT * FROM announcements WHERE id = %d", $announceid);

//echo $sql;

$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

$num_rows = $result->num_rows;

if ( $num_rows < 1 )  {
	$projectname = "ANNNOUNCEMENT ID NOT FOUND";
}  else  {
	$row = $result->fetch_assoc();
	$announcesubject = $row['subject'];
	$announceauthor = $row['username'];
	$announcedate = $row['date'];
	$announcebody = $row['text'];
}
$conn->close();


?>

<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Announcements</title>
<style type="text/css">
<!--
.style37 {font-family: Arial, Helvetica, sans-serif; font-size: 14; }
.style38 {font-size: 14}
-->
</style>
</head>

<body>
<form name="form1" method="post" action="announceDoDelete.php">

<?php


echo "<input name=\"id\" type=\"hidden\" value=";
echo $announceid;
echo ">";


?>

<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Annnouncement Details</td>
        </tr>
        <tr class="maintext">
          <td width="21%">&nbsp;</td>
        </tr>
        
        <tr class="maintext">
          <td><table width="70%" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#CCCCCC" class="dialogbox">
            <tr>
              <td width="50%" class="style37"><p class="style37">Announcement Author: </p></td>
              <td width="50%">

<?php

	echo $announceauthor;


?>              </td>
            </tr>
            <tr>
              <td class="style37">Annnouncement Date:</td>
              <td>
              
<?php


	echo $announcedate;
	

?>              </td>
            </tr>
            <tr>
              <td class="style37">Announcement Subject: </td>
              <td>

<?php


echo $announcesubject;


?>              </td>
            </tr>
            <tr>
              <td class="style37">Annnouncement:</td>
              <td>&nbsp;</td>
            </tr>
            
            <tr>
              <td colspan="2" class="style37"><span class="style38"></span><span class="style38">
<?php


echo $announcebody;


?>
              </span></td>
              </tr>
            
            
            

            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37"><label>
                  <div align="center">                    
                     <input name="deleteproject" type="submit" class="dialogboxbuttons" id="deleteproject" value="     DELETE THIS ANNOUNCEMENT     " />
                  </div>
                </label></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
  </tr>
</table>
</form>
</body>
</html>
