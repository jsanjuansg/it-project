<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$username = $_SESSION['itp_username'];
$chat = trim($_POST['chat']);
$to = trim($_POST['to']);

$currentdate = date('Y-m-d H:i:s');
$id = date('Y-m-d');

if ( empty($chat) )  {
	header('Location: index.phpchatFrameBottom.php');
}

if ( empty($to) )  {
	header('Location: index.phpchatFrameBottom.php');
}


$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use db : ' . $conn->error);
}

$chat = $conn->real_escape_string($chat);
$username = $_SESSION['itp_username'];
$read = 0;

/*

id
fromuser
cdate
message
touser
cread

*/

$sql = sprintf("INSERT INTO chat(id,fromuser,cdate,message,touser,cread) VALUES('%s','%s','%s','%s','%s',%d)", $id,$username,$currentdate,$chat,$to,$read);

//echo $sql;

$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

//  mark messages to me as read 
$sql2 = sprintf("UPDATE chat SET cread = 1 WHERE touser = '%s' AND fromuser = '%s' AND id = '%s'", $username, $to, $id);

$result = $conn->query($sql2);
if (!$result2) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql2;
    die($message);
}

$conn->close();

$redirect="Location: index.phpchatFrameBottom.php?to=";
$redirect .= $to;

header($redirect);

?>