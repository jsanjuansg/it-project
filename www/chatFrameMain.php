<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$username = $_SESSION['itp_username'];
$to = $_GET['to'];
$id = date('Y-m-d');

?>

<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project Portal: Chat</title>
</head>
<!-- Codes by Quackit.com -->
<html>
<script type="text/JavaScript">
<!--
function timedRefresh(timeoutPeriod) {
	setTimeout("location.reload(true);",timeoutPeriod);
}
//   -->
</script>
</head>


<body onLoad="JavaScript:timedRefresh(4000);">


<table width="90%" border="0" cellspacing="0" cellpadding="1" align="center">

<?php

$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use DB : ' . $conn->error);
}


/*

id
fromuser
cdate
message
touser

*/

$sql = sprintf("SELECT fromuser,touser,cdate,message FROM chat WHERE id = '%s' AND (fromuser = '%s' OR fromuser = '%s') AND (touser ='%s' OR touser = '%s') ORDER BY cdate LIMIT 50", $id, $username,$to,$username,$to);


//echo $sql;


$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

while ( $row = $result->fetch_assoc() ) {
	echo "<tr>\n";
	
	echo "<td class=\"maintext\" width=\"10%\">\n";
	echo $row['fromuser'];
	echo "</td>\n";

/*	
	echo "<td class=\"maintext\" width=\"10%\">\n";
	echo $row['cdate'];
	echo "</td>\n";
*/

	echo "<td class=\"maintext\">\n";
	echo $row['message'];
	echo "</td>\n";
	
	echo "</tr>\n";
}


//  mark messages to me as read 
$sql2 = sprintf("UPDATE chat SET cread = 1 WHERE touser = '%s' AND fromuser = '%s' AND id = '%s'", $username, $to, $id);

$result = $conn->query($sql2);
if (!$result2) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql2;
    die($message);
}


 
$conn->close();


?>
  
</table>

</body>
</html>
