<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$username = $_SESSION['itp_username'];

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Calendar Main</title>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain"><img src="img/chat.gif" width="48" height="48"> Chat</td>
        </tr>
        <tr class="maintext">
          <td width="21%">&nbsp;</td>
        </tr>
        
        <tr class="maintext">
          <td><table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
            <tr>
              <td width="10%" class="menubar">Online</td>
              <td width="20%" class="menubar">&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
            

<?php

$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 

/*

id
date
username
project_id
text
privacy


*/
$sql = sprintf("SELECT username,firstname,lastname FROM users WHERE online = 1 AND username <> '%s'", $username);


//echo $sql;


$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

while ( $row = $result->fetch_assoc() ) {
	echo "<tr>\n";
	
	echo "<td>\n";
	echo "<a href=\"chatFrame.php?to=";
	echo $row['username'];
	echo "\">";
	echo $row['username'];
	echo "</a>";
	echo "</td>\n";
	
	echo "<td>\n";
	echo $row['firstname'];
	echo "&nbsp;";
	echo $row['lastname'];
	echo "</td>\n";
	
	echo "</tr>\n";
}

 
$conn->close();


?>
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </tr>
</table>
</body>
</html>
