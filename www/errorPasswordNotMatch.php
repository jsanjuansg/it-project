<html>

<?php

require_once "itproject.php";

?>

<head>
<title>IT Project: Login</title>
<link rel="stylesheet" href="itproject.css" type="text/css">
<script language="JavaScript" src="itproject.js">
</script>
</head>

<!--
1000 milliseconds in one second for setTimeout
-->

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>     
      <table width="40%" border="0" align="center" cellpadding="2" cellspacing="0" class="loginerrorsubtitlebar">
      <tr>
        <td><table width="100%" border="0" align="center" cellpadding="10" cellspacing="0">
          <tr>
            <td colspan="2" class="loginerrorsubtitlebar">Error!  Passwords do not match.</td>
            </tr>
          <tr class="loginsubtitlecontent">
            <td colspan="2">The two passwords you have entered does not match.</td>
          </tr>        
        </table></td>
        
</table>
</body>
</html>
