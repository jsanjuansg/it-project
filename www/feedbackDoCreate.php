<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}


$feedbacksubject = trim($_POST['feedbacksubject']);
$feedbackbody = trim($_POST['feedbackbody']);

$currentdate = date('Y-m-d');

if ( empty($feedbacksubject) )  {
	header('Location: index.phperrorFieldsMissing.php');
}

if ( empty($feedbackbody) )  {
	header('Location: index.phperrorFieldsMissing.php');
}


$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}

$feedbacksubject = $conn->real_escape_string($feedbacksubject);
$feedbackbody = $conn->real_escape_string($feedbackbody);
$username = $_SESSION['itp_username'];
$projectid = 0;
$privacy = 1;

/*

id
date
username
text
project_id
privacy ( 0 = private; 1 = public)
subject

mysql_insert_id 

*/

$sql = sprintf("INSERT INTO feedback (id,date,username,text,project_id,privacy,subject) VALUES(null,'%s','%s','%s', '%s', '%s', '%s')", $currentdate,$username,$feedbackbody,$project_id,$privacy,$feedbacksubject);

//echo $sql;

$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $query;
    die($message);
}
$conn->close();


?>

<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Profiles Main</title>
<style type="text/css">
<!--
.style37 {font-family: Arial, Helvetica, sans-serif; font-size: 14; }
.style38 {font-size: 14}
-->
</style>
<script language="JavaScript" src="itproject.js">
</script>
</head>

<body onLoad="setTimeout('feedbackredirect()', 3000)">
<form name="form1" method="post" action="projectDoCreate.php">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Feedback Created:</td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td width="21%">You will now be redirected to the Feedback Page in 3 seconds. If not, click <a href="feedbackMain.php">here</a> </td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="70%" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#CCCCCC">
            <tr>
              <td width="50%" class="style37"><p class="style37">Subject:</p></td>
              <td width="50%">

<?php

	echo $feedbacksubject;


?>              </td>
            </tr>
            <tr>
              <td class="style37">Body:</td>
              <td>
              
<?php


	echo $feedbackbody;
	

?>              </td>
            </tr>
                    
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37"><label>
                  <div align="center"></div>
                </label></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
  </tr>
</table>
</form>
</body>
</html>
