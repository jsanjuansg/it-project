<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Feedback Main</title>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain"> <img src="img/feedback.gif" width="48" height="48"> Feedback</td>
        </tr>
        <tr class="maintext">
          <td width="21%"><a href="feedbackCreateNew.php">Create New Feedback</a></td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
            <tr>
              <td width="10%" class="menubar">Date/Time</td>
              <td width="20%" class="menubar">Subject</td>
            </tr>
            <tr>
            

<?php

$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 

/*

id
date
username
project_id
text
privacy
subject

*/
$sql = "SELECT id,date,text,subject FROM feedback";

//echo $sql;


$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

while ( $row = $result->fetch_assoc() ) {
	echo "<tr>\n";
	
	echo "<td>\n";
	echo $row['date'];
	echo "</td>\n";
	
	echo "<td>\n";
	$announce_url = "<a href=\"" . $itproject_url . "/";
	$announce_url .= "feedbackView.php?id=" . $row['id'];
	$announce_url .= "\">";
	echo $announce_url;
	echo $row['subject'];
	echo "</a>";

	echo "</td>\n";
	
	echo "</tr>\n";
}

 
$conn->close();


?>

            
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </tr>
</table>
</body>
</html>
