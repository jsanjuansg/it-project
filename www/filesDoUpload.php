<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}


$username = $_SESSION['itp_username'];
$uploaddir = '/home/itprojec/public_html/files/';
$filename = basename($_FILES['file']['name']);
$uploadfile = $uploaddir . $filename;


if ( !move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile) ) {
	header('Location: index.phperrorFileUpload.php');
}  else  {

	$description = trim($_POST['description']);
	$currentdate = date('Y-m-d H:i:s');

	if ( empty($description) )  {
		header('Location: index.phperrorFieldsMissing.php');
	}


	$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
	if ($conn->connect_error) {
    	die('Could not connect: ' . $conn->error);
	}

	 
	 
    	die ('Can\'t use foo : ' . $conn->error);
	}

	$description = $conn->real_escape_string($description);
	$projectid = 1;
	$privacy = 1;

/*

name
username
project_id
fdate
privacy
description

*/

	$sql = sprintf("INSERT INTO files(name,username,project_id,fdate,privacy,description) VALUES('%s','%s',%d,'%s',%d,'%s')", $filename,$username,$projectid,$currentdate,$privacy,$description);

//echo $sql;

	$result = $conn->query($sql);
	if (!$result) {
    	$message  = 'Invalid query: ' . $conn->error . "\n";
    	$message .= 'Whole query: ' . $sql;
    	die($message);
	}

	$conn->close();

}

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Files Upload</title>
<style type="text/css">
<!--
.style37 {font-family: Arial, Helvetica, sans-serif; font-size: 14; }
.style38 {font-size: 14}
-->
</style>
<script language="JavaScript" src="itproject.js">
</script>
</head>

<body onLoad="setTimeout('filesredirect()', 3000)">
<form name="form1" method="post" action="filesDoUpload.php" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Projects : Upload File</td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td width="21%">You will now be redirected to the Files Main Page in 3 seconds. If not, click <a href="filesMain.php">here</a> </td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="70%" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#CCCCCC">
            <tr>
              <td width="50%" class="style37"><p class="style37">File: *</p></td>
              <td width="50%"><span class="style38">
                <label>

<?php

echo $filename;


?>
                </label>
              </span></td>
            </tr>
            <tr>
              <td class="style37">Description: *</td>
              <td><span class="style38">
<?php

echo $description;

?>
              </span></td>
            </tr>
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">* required fields</td>
            </tr>        
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
  </tr>
</table>
</form>
</body>
</html>
