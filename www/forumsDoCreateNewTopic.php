<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}


$forumsubject = trim($_POST['forumsubject']);
$forumbody = trim($_POST['forumbody']);

$currentdate = date('Y-m-d');

if ( empty($forumsubject) )  {
	header('Location: index.phperrorFieldsMissing.php');
	exit;
}

if ( empty($forumbody) )  {
	header('Location: index.phperrorFieldsMissing.php');
	exit;
}


$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}

$forumsubject = $conn->real_escape_string($forumsubject);
$forumbody = $conn->real_escape_string($forumbody);
$username = $_SESSION['itp_username'];
$projectid = 0;
$privacy = 1;

/*

id
projects_id
subject
body
date_created
privacy


*/

$sql = sprintf("INSERT INTO forums (id,projects_id,subject,body,date_created,privacy,author) VALUES(null,%d,'%s','%s', '%s', %d,'%s')", $project_id,$forumsubject,$forumbody,$currentdate,$privacy,$username);

//echo $sql;

$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}
$conn->close();


?>

<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Forums Main</title>
<style type="text/css">
<!--
.style37 {font-family: Arial, Helvetica, sans-serif; font-size: 14; }
.style38 {font-size: 14}
-->
</style>
<script language="JavaScript" src="itproject.js">
</script>
</head>

<body onLoad="setTimeout('forumsredirect()', 3000)">
<form name="form1" method="post" action="projectDoCreate.php">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">New Forum Topic Created:</td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td width="21%">You will now be redirected to the Forums Page in 3 seconds. If not, click <a href="forumsMain.php">here</a> </td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="70%" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#CCCCCC">
            <tr>
              <td width="50%" class="style37"><p class="style37">Subject:</p></td>
              <td width="50%">

<?php

	echo $forumsubject;


?>              </td>
            </tr>
            <tr>
              <td class="style37">Body:</td>
              <td>
              
<?php


	echo $forumbody;
	

?>              </td>
            </tr>
                    
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37"><label>
                  <div align="center"></div>
                </label></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
  </tr>
</table>
</form>
</body>
</html>
