<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}


$id = $_POST['id'];

if ( empty($id) )  {
	header('Location: index.phpforumsMain.php');
	exit;
}


$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use DB : ' . $conn->error);
}

$sql1 = sprintf("DELETE FROM forums WHERE id = %d", $id);

//echo $sql1;

$result1 = mysql_query($sql1, $link);
if (!$result1) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql1;
    die($message);
}

$sql2 = sprintf("DELETE FROM forums_messages WHERE forums_id = %d", $id);

//echo $sql1;

$result = $conn->query($sql2);
if (!$result2) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql2;
    die($message);
}


$conn->close();


?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Forums Main</title>
<script language="JavaScript" src="itproject.js">
</script>
</head>

<body onLoad="setTimeout('forumsredirect()', 3000)">

<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Forum Topic Deleted</td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td>You will now be redirected to the Forums Main Page in 3 seconds. If not, click <a href="forumsMain.php">here</a> </td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </tr>
</table>

</body>
</html>
