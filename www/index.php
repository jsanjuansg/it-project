<html>

<?php

require_once "itproject.php";

?>

<head>
<title>IT Project: Login</title>
<link rel="stylesheet" href="itproject.css" type="text/css">
</head>

<body>
<form name="login" method="post" action="login.php">
<table width="100%" border="0" cellspacing="0" cellpadding="10">
  <tr class="logintitlebar">
    <td height="56">

<?php

echo $apptitlebar;

?>    
    
    
    </td>
    <td><span class="logindate">Today is 

<?php

	echo date('D F, j, Y');

?>    
    
    </span>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <p>&nbsp;</p>
      <table width="40%" border="0" align="center" cellpadding="2" cellspacing="0" class="loginsubtitlebar">
      <tr>
        <td><table width="100%" border="0" align="center" cellpadding="10" cellspacing="0">
          <tr>
            <td colspan="2" class="loginsubtitlebar">Please Login</td>
            </tr>
          <tr class="loginsubtitlecontent">
            <td>Username</td>
            <td>
              <input type="text" name="username" id="username">
            </td>
          </tr>
          <tr class="loginsubtitlecontent">
            <td>Password</td>
            <td><input type="password" name="password" id="password"></td>
          </tr>
          <tr class="loginsubtitlecontent">
            <td colspan="2" >
              <div align="right">
                <input type="submit" name="login" id="login" value="    Login    ">
                </div>
            </td>
            </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</form>
</body>
</html>
