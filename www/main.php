<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Main</title>
<script type="text/JavaScript">
<!--
function timedRefresh(timeoutPeriod) {
	setTimeout("location.reload(true);",timeoutPeriod);
}
//   -->
</script>

</head>

<body onLoad="JavaScript:timedRefresh(30000);">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2">
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Announcements</td>
        </tr>
        
<?php

$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 

/*

id
date
username
text
project_id
privacy
subject

*/

$sql = "SELECT id,date,subject FROM announcements ORDER BY date DESC LIMIT 4";

//echo $sql;


$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

while ( $row = $result->fetch_assoc() ) {
	echo "<tr class=\"maintext\">\n";
	
	echo "<td>\n";
	echo $row['date'];
	echo "</td>\n";
	
	echo "<td>\n";
	$announce_url = "<a href=\"" . $itproject_url . "/";
	$announce_url .= "announceView.php?id=" . $row['id'];
	$announce_url .= "\">";
	echo $announce_url;
	echo $row['subject'];
	echo "</a>";

	echo "</td>\n";
	

//	status (1 = done; 2 = ongoing; 3 = onhold ; 4 = cancelled)


	echo "</tr>\n";
}

 
$conn->close();


?>

      <tr>
      	<td class="maintext">
        <a href="announceMain.php">See more Announcements </a>
        </td>
      </tr>  
      </table>
      <p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Events</td>
        </tr>
<?php

$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 
 

/*

id
date
username
project_id
text
privacy


*/
$sql = "SELECT id,date,text FROM calendar ORDER BY date DESC LIMIT 4";

//echo $sql;


$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

while ( $row = $result->fetch_assoc() ) {
	echo "<tr class=\"maintext\">\n";
	
	echo "<td>\n";
	echo $row['date'];
	echo "</td>\n";
	
	echo "<td>\n";
	$event_url = "<a href=\"" . $itproject_url . "/";
	$event_url .= "eventsView.php?id=" . $row['id'];
	$event_url .= "\">";
	echo $event_url;
	echo $row['text'];
	echo "</a>";

	echo "</td>\n";
	
	echo "</tr>\n";
}

 
$conn->close();


?>
      <tr>
      	<td class="maintext">
        <a href="eventsMain.php">See more Events</a>
        </td>
      </tr>  

      </table>
      <p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">News</td>
        </tr>
<?php

$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 

/*

id
date
username
text
project_id
privacy
subject

*/

$sql = "SELECT id,date,subject FROM news ORDER BY date DESC LIMIT 4";

//echo $sql;


$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

while ( $row = $result->fetch_assoc() ) {
	echo "<tr class=\"maintext\">\n";
	
	echo "<td>\n";
	echo $row['date'];
	echo "</td>\n";
	
	echo "<td>\n";
	$news_url = "<a href=\"" . $itproject_url . "/";
	$news_url .= "newsView.php?id=" . $row['id'];
	$news_url .= "\">";
	echo $news_url;
	echo $row['subject'];
	echo "</a>";

	echo "</td>\n";
	

//	status (1 = done; 2 = ongoing; 3 = onhold ; 4 = cancelled)


	echo "</tr>\n";
}

 
$conn->close();


?>

      <tr>
      	<td class="maintext">
        <a href="newsMain.php">See more News</a>
        </td>
      </tr>  

      </table>
      <p>&nbsp;</p>
            <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Projects</td>
        </tr>
<?php

$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}




$sql = "SELECT id,end_date,name FROM projects ORDER BY end_date LIMIT 5";

//echo $sql;


$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

while ( $row = $result->fetch_assoc() ) {
	echo "<tr class=\"maintext\">\n";
	
	echo "<td>\n";
	$file_url = "<a href=\"" . $itproject_url . "/";
	$file_url .= "projectsView.php?id=" . $row['id'];
	$file_url .= "\">";
	echo $file_url;
	echo $row['name'];
	echo "</a>";

	echo "</td>\n";
	
	echo "<td>\n";
	echo $row['end_date'];
	echo "</td>\n";
	
	echo "</tr>\n";
}

 
$conn->close();


?>

      <tr>
      	<td class="maintext">
        <a href="projectsMain.php">See more Projects</a>
        </td>
      </tr>  
      </table>
    <p>&nbsp;</p></td>

<!--    
               <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Files</td>
        </tr>
<?php

/*
$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 


$sql = "SELECT name,fdate FROM files ORDER BY fdate DESC LIMIT 4";

//echo $sql;


$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

while ( $row = $result->fetch_assoc() ) {
	echo "<tr class=\"maintext\">\n";
	
	echo "<td>\n";
	$file_url = "<a href=\"" . $itproject_url . "/";
	$file_url .= "files/" . $row['name'];
	$file_url .= "\">";
	echo $file_url;
	echo $row['name'];
	echo "</a>";

	echo "</td>\n";
	
	echo "<td>\n";
	echo $row['fdate'];
	echo "</td>\n";
	
	echo "</tr>\n";
}

 
$conn->close();

*/

?>


      <tr>
      	<td class="maintext">
        <a href="filesMain.php">See more Files</a>
        </td>
      </tr>  
      </table>
-->
      
    <p>&nbsp;</p></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
</table>
</body>
</html>
