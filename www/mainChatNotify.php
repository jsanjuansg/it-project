<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #203360;
}
-->
</style>
<title>IT Project: Main Chat Notifier</title>
<script type="text/JavaScript">
<!--
function timedRefresh(timeoutPeriod) {
	setTimeout("location.reload(true);",timeoutPeriod);
}
//   -->
</script>
</head>

<body onLoad="JavaScript:timedRefresh(8000);">
<table width="100%" border="0" cellspacing="0" cellpadding="5" class="menubar">
  <tr class="chatnotify">
    <td class="chatnotifytitle">Chat Notify:</td>
  </tr>


<?php

$username = $_SESSION['itp_username'];

$currentdate = date('Y-m-d H:i:s');
$id = date('Y-m-d');
$read = 0;

$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 

/*

id
fromuser
cdate
message
touser
cread

*/

$sql = sprintf("SELECT DISTINCT id,fromuser FROM chat WHERE touser = '%s' AND id = '%s' AND cread = %d", $username, $id, $read);

//echo $sql;

$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

while ( $row = $result->fetch_assoc() ) {
	echo "<tr class=\"chatnotify\">\n";
	
	echo "<td class=\"chatnotify\">\n";
	
	$chat_url = "<a href=\"" . $itproject_url . "/";
	$chat_url .= "chatFrame.php?to=" . $row['fromuser'];
	$chat_url .= "\" class=\"chatnotify\" target=\"main\">";
	
	echo $chat_url;
	echo $row['fromuser'];
	echo "</a>";
	echo "</td>\n";
	
	echo "</tr>\n";
}

 
$conn->close();


?>
  
  
</table>
</body>

</html>
