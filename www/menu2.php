<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #627aad;
}
-->
</style>
<title>IT Project: Menu</title>
</head>

<body id="menu">
<p>&nbsp;</p>
<table width="120" border="0" cellspacing="0" cellpadding="3" class="menubar">
  <tr class="menubar">
    <td><a href="main.php" target="main" class="menubarlink">Main</a></span></td>
  </tr>
  <tr>
    <td><a href="projectsMain.php" target="main" class="menubarlink">Projects</span></a></td>
  </tr>
  <tr>
    <td><a href="filesMain.php" target="main" class="menubarlink">Files</a></span></td>
  </tr>
  <tr>
    <td><a href="newsMain.php" target="main" class="menubarlink">News</span></a></td>
  </tr>
  <tr>
    <td><a href="announceMain.php" target="main" class="menubarlink">Announcements</a></td>
  </tr>
  <tr>
    <td><a href="eventsMain.php" target="main" class="menubarlink">Events</a></td>
  </tr>
  <tr>
    <td><a href="feedbackMain.php" target="main" class="menubarlink">Feedback</a></td>
  </tr>
  <tr>
    <td><a href="forumsMain.php" target="main" class="menubarlink">Forums</a></span></td>
  </tr>
  <tr>
    <td><a href="chatMain.php" target="main" class="menubarlink">Chat</a></span></td>
  </tr>
  <tr>
    <td><a href="profilesMain.php" target="main" class="menubarlink">Profile</a></span></td>
  </tr>
  <tr>
    <td><a href="helpMain.php" target="main" class="menubarlink">Help</a></span></td>
  </tr>
  <tr>
    <td><a href="logout.php" target="_top" class="menubarlink">Logout</a></span></td>
  </tr>
</table>
</body>
</html>
