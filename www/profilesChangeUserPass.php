<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$username = $_SESSION['itp_username'];

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Profiles Main</title>
</head>

<body>
<form name="form1" method="post" action="profilesDoChangeUserPass.php">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="3" class="loginsubtitlebarmain">Change My Password</td>
        </tr>
        <tr class="maintext">
          <td colspan="2" class="dialogbox">&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td width="29%" class="dialogbox">Username</td>
          <td width="71%" class="dialogbox">
          
<?php


echo $username;


?>          </td>
        </tr>
        <tr class="maintext">
          <td class="dialogbox">Old Password: *</td>
          <td class="dialogbox"><input type="password" name="oldpassword" id="oldpassword"></td>
        </tr>
        <tr class="maintext">
          <td class="dialogbox">New Password: *</td>
          <td class="dialogbox"><input type="password" name="password1" id="password1"></td>
        </tr>
        <tr class="maintext">
          <td class="dialogbox">Retype New Password: *</td>
          <td class="dialogbox"><input type="password" name="password2" id="password2"></td>
        </tr>
        <tr class="maintext">
          <td class="dialogbox">&nbsp;</td>
          <td class="dialogbox">&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td class="dialogbox">* required fields</td>
          <td class="dialogbox">&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td class="dialogbox">&nbsp;</td>
          <td class="dialogbox">&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td colspan="2" class="dialogbox"><input name="submit" type="submit" class="dialogboxbuttons" id="submit" value="     CHANGE MY PASSWORD     "></td>
        </tr>
      </table>
      <p>&nbsp;</p>


<?php

if ( $_SESSION['itp_username'] == "administrator" )  {

print <<< END
      
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Profiles</td>
        </tr>
        <tr class="maintext">
          <td width="22%"><a href="profilesAdd.php">Add New Profile</a></td>
        </tr>      
        <tr class="maintext">
          <td width="22%">Change Password</td>
        </tr>      
        <tr class="maintext">
          <td width="22%">Modify Profile</td>
        </tr>      
      </table>

END;

}


?>
      
      
  </tr>
</table>
</form>
</body>
</html>
