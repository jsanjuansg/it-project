<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$username = trim($_POST['username']);
$password1 = trim($_POST['password1']);
$password2 = trim($_POST['password2']);
$firstname = trim($_POST['firstname']);
$lastname = trim($_POST['lastname']);
$email = trim($_POST['email']);
$role = trim($_POST['role']);


if ( empty($username) )  {
	header('Location: index.phperrorFieldsMissing.php');
}

if ( $password1 != $password2 )  {
	header('Location: index.phperrorPasswordNotMatch.php');
}

$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}

$username = $conn->real_escape_string($username);
$password1 = $conn->real_escape_string($password1);
$password2 = $conn->real_escape_string($password2);
$firstname = $conn->real_escape_string($firstname);
$lastname = $conn->real_escape_string($lastname);
$email = $conn->real_escape_string($email);
$role = $conn->real_escape_string($role);
$group = 0;
$online = 0;


/*

username
firstname
lastname
email
position_id
group_id
password
online

*/

$sql = sprintf("INSERT INTO users (username,firstname,lastname,email,position_id,group_id,password,online) VALUES('%s','%s','%s','%s', %d, %d, '%s', %d)",$username,$firstname,$lastname,$email,$role,$group,$password1,$online);

//echo $sql;


$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $query;
    die($message);
}

$conn->close();


?>
<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Profiles Added</title>
<script language="JavaScript" src="itproject.js">
</script>
</head>

<body onLoad="setTimeout('profilesredirect()', 3000)">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>     
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">New Profile Added</td>
        </tr>
        <tr class="maintext">
          <td width="24%">Username: </td>
          <td width="76%">
<?php

	echo $username;

?>          
          
          </td>
        </tr>  
         <tr class="maintext">
          <td width="24%">First Name: </td>
          <td width="76%">

<?php

	echo $firstname;

?>          

          
          </td>
         </tr>      
         <tr class="maintext">
           <td>Last Name: </td>
           <td>
           
<?php

	echo $lastname;

?>             
           
           </td>
         </tr>
        <tr class="maintext">
          <td width="24%"> Password: </td>
          <td width="76%">
          
<?php

	echo $password1;

?>          
          
          </td>
        </tr>      
        <tr class="maintext">
          <td>E-mail address: </td>
          <td>

<?php


	echo $email;

?>          
          
          </td>
        </tr>
        <tr class="maintext">
          <td>Role: </td>
          <td>
          
<?php

if ( $role == 0 )  {
	echo "Staff";
}  else  {
	echo "Manager";
}	

?>
          
          </td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td colspan="2">You will now be redirected to the Profiles Page in 3 seconds. If not, click <a href="profilesMain.php">here</a> </td>
        </tr>      
      </table>
  </tr>
</table>
</form>
</body>
</html>