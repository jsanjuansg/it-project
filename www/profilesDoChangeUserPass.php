<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$username = $_SESSION['itp_username'];
$oldpassword = trim($_POST['oldpassword']);
$password1 = trim($_POST['password1']);
$password2 = trim($_POST['password2']);

if ( empty($username) )  {
	header('Location: index.phperrorFieldsMissing.php');
}

if ( empty($oldpassword) )  {
	header('Location: index.phperrorFieldsMissing.php');
}

if ( $password1 != $password2 )  {
	header('Location: index.phperrorPasswordNotMatch.php');
}

$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}

$username = $conn->real_escape_string($username);
$oldpassword = $conn->real_escape_string($oldpassword);
$password1 = $conn->real_escape_string($password1);
$password2 = $conn->real_escape_string($password2);

$sql = sprintf("SELECT password FROM users WHERE username='%s'", $username);


$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}
$row = $result->fetch_assoc();
$epass = $row['password'];

if ( $oldpassword != $epass )  {
	header('Location: index.phperrorIncorrectPassword.php');
}


/*

username
firstname
lastname
email
position_id
group_id
password
online

*/

$sql = sprintf("UPDATE users SET password = '%s' WHERE username = '%s'",$password1,$username); 

//echo $sql;


$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

$conn->close();


?>
<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Profiles Added</title>
<script language="JavaScript" src="itproject.js">
</script>
</head>

<body onLoad="setTimeout('profilesredirect()', 3000)">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>     
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Your Password Has Been Changed</td>
        </tr>
        <tr class="maintext">
          <td width="24%">Username: </td>
          <td width="76%">
<?php

	echo $username;

?>          
          
          </td>
        </tr>  
        <tr class="maintext">
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td colspan="2">You will now be redirected to the Profiles Page in 3 seconds. If not, click <a href="profilesMain.php">here</a> </td>
        </tr>      
      </table>
  </tr>
</table>
</form>
</body>
</html>