<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$username = $_SESSION['itp_username'];
$firstname = trim($_POST['firstname']);
$lastname = trim($_POST['lastname']);
$email = trim($_POST['email']);

if ( empty($firstname) )  {
	header('Location: index.phperrorFieldsMissing.php');
	exit;
}

if ( empty($lastname) )  {
	header('Location: index.phperrorFieldsMissing.php');
	exit;
}

if ( empty($email) )  {
	header('Location: index.phperrorFieldsMissing.php');
	exit;
}


$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}

$firstname = $conn->real_escape_string($firstname);
$lastname = $conn->real_escape_string($lastname);
$email = $conn->real_escape_string($email);

/*

username
firstname
lastname
email
position_id
group_id
password
online

*/

$sql = sprintf("UPDATE users SET firstname = '%s', lastname = '%s', email = '%s' WHERE username = '%s'",$firstname,$lastname,$email,$username);

//echo $sql;


$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $query;
    die($message);
}

$conn->close();


?>
<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Profiles Added</title>
<script language="JavaScript" src="itproject.js">
</script>
</head>

<body onLoad="setTimeout('profilesredirect()', 3000)">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>     
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Your Profile Has Been Modified</td>
        </tr>
        <tr class="maintext">
          <td width="24%">Username: </td>
          <td width="76%">
<?php

	echo $username;

?>          
          
          </td>
        </tr>  
         <tr class="maintext">
          <td width="24%">First Name: </td>
          <td width="76%">

<?php

	echo $firstname;

?>          

          
          </td>
         </tr>      
         <tr class="maintext">
           <td>Last Name: </td>
           <td>
           
<?php

	echo $lastname;

?>             
           
           </td>
         </tr>
        <tr class="maintext">
          <td width="24%"> Password: </td>
          <td width="76%">
          
<?php

	echo $password1;

?>          
          
          </td>
        </tr>      
        <tr class="maintext">
          <td>E-mail address: </td>
          <td>

<?php


	echo $email;

?>          
          
          </td>
        </tr>
        <tr class="maintext">
          <td>Role: </td>
          <td>
          
<?php

if ( $role == 0 )  {
	echo "Staff";
}  else  {
	echo "Manager";
}	

?>
          
          </td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td colspan="2">You will now be redirected to the Profiles Page in 3 seconds. If not, click <a href="profilesMain.php">here</a> </td>
        </tr>      
      </table>
  </tr>
</table>
</form>
</body>
</html>