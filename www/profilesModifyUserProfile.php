<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$username = $_SESSION['itp_username'];

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Profiles Add New</title>
</head>

<body>

<?php


$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}


/*

username
firstname
lastname
email
position_id
group_id
password
online

*/

$sql = sprintf("SELECT * FROM users WHERE username = '%s'", $username);

//echo $sql;

$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

$row = $result->fetch_assoc();


?>


<form name="form1" method="post" action="profilesDoModifyUserProfile.php">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>     
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Modify My Profile:</td>
        </tr>
        <tr class="maintext">
          <td width="24%" class="dialogbox">Username: *</td>
          <td width="76%" class="dialogbox">
            <label>

<?php

echo $username

?>            
            
            </label>          </td>
        </tr>  
         <tr class="maintext">
          <td width="24%" class="dialogbox">First Name: *</td>
          <td width="76%" class="dialogbox">
		  
<?php
		  
echo "<input type=\"text\" name=\"firstname\" value=\"";
echo $row['firstname'];
echo "\">";

?>

<br>
		  </td>
        </tr>      
         <tr class="maintext">
           <td class="dialogbox">Last Name: *</td>
           <td class="dialogbox">
           
           
<?php
           
echo "<input type=\"text\" name=\"lastname\" value=\"";
echo $row['lastname'];
echo "\">";

		   
?>           </td>
         </tr>
              
        
        <tr class="maintext">
          <td class="dialogbox">E-mail address: *</td>
          <td class="dialogbox">
          
<?php
          
  
echo "<input type=\"text\" name=\"email\" value=\"";
echo $row['email'];
echo "\">";

          
?>          </td>
        </tr>
        <tr class="maintext">
          <td class="dialogbox">&nbsp;</td>
          <td class="dialogbox">&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td class="dialogbox">* required field</td>
          <td class="dialogbox">&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td class="dialogbox">&nbsp;</td>
          <td class="dialogbox">&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td colspan="2" class="dialogbox"><input name="submit" type="submit" class="dialogboxbuttons" id="submit" value="     Modify My Profile     "></td>
          </tr>      
      </table>
  </tr>
</table>
</form>

<?php

$conn->close();

?>


</body>
</html>
