<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$id = $_POST['id'];
$projectname = trim($_POST['projectname']);
$projectdescription = trim($_POST['projectdescription']);
$projectmembers = $_POST['projectmembers'];

$count_members = count($projectmembers);

$smonth = $_POST['smonth'];
$sday = $_POST['sday'];
$syear = $_POST['syear'];

$emonth = $_POST['emonth'];
$eday = $_POST['eday'];
$eyear = $_POST['eyear'];

$sdatestamp = mktime(0,0,0,$smonth,$sday,$syear);
$edatestamp = mktime(0,0,0,$emonth,$eday,$eyear);

$sdate = date('Y-m-d', $sdatestamp);
$edate = date('Y-m-d', $edatestamp);

if ( !$edatestamp )  {
	header('Location: index.phperrorInvalidDate.php');
	exit();
}

if ( !$sdatestamp )  {
	header('Location: index.phperrorInvalidDate.php');
	exit();
}

if ( $count_members < 1 )  {
	header('Location: index.phperrorNoMembers.php');
	exit();
}

if ( empty($projectname) )  {
	header('Location: index.phperrorFieldsMissing.php');
	exit();
}


$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}

$projectname = $conn->real_escape_string($projectname);
$projectdescription = $conn->real_escape_string($projectdescription);
$username = $_SESSION['itp_username'];

/*

id
name
manager
start_date
end_date
actual_start_date
actual_end_date
status (1 = done; 2 = ongoing; 3 = onhold ; 4 = cancelled)
privacy ( 0 = private; 1 = public)
description

mysql_insert_id 

mktime(0,0,0,mon,day,year);

*/

$sql = sprintf("UPDATE projects SET name = '%s', start_date = '%s', end_date = '%s', description = '%s' WHERE id = %d", $projectname,$sdate,$edate,$projectdescription,$id);

//echo $sql;

$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

//  delete all existing project members
$sql3 = sprintf("DELETE FROM projects_members WHERE project_id = %d", $id);
$result3 = mysql_query($sql3, $link);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql3;
    die($message);
}


/*

insert updated project members

projects_members
project_id
username

*/

$i = 0;

//echo $count_members;
//echo "\n\n";

while ( $count_members > $i )  {

	$sql2 = sprintf("INSERT INTO projects_members(project_id,username) VALUES(%d,'%s')", $id, $projectmembers[$i]);	
	$result = $conn->query($sql2);

	if (!$result2) {
		$message = "SQL: " . $sql2 . "\n\n";
    	$message  .= 'Invalid query: ' . $conn->error . "\n";
    	$message .= 'Whole query: ' . $sql2;
		die($message);
	}
	$i++;
	
}

$conn->close();


?>

<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Profiles Main</title>
<style type="text/css">
<!--
.style37 {font-family: Arial, Helvetica, sans-serif; font-size: 14; }
.style38 {font-size: 14}
-->
</style>
<script language="JavaScript" src="itproject.js">
</script>
</head>

<body onLoad="setTimeout('projectsredirect()', 3000)">
<form name="form1" method="post" action="projectDoCreate.php">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Project Updated</td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td width="21%">You will now be redirected to the Projects Page in 3 seconds. If not, click <a href="projectsMain.php">here</a>
          </td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="70%" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#CCCCCC">
            <tr>
              <td width="50%" class="style37"><p class="style37">Project Name: </p></td>
              <td width="50%">

<?php

	echo $projectname;


?>              </td>
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
  </tr>
</table>
</form>
</body>
</html>
