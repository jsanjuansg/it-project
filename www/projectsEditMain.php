<?php

require_once "itproject.php";

session_start();
if ( !isset($_SESSION['itp_username']) )  {
	header('Location: index.php');
}

$id = $_GET['id'];

?>


<html>
<head>
<link rel="stylesheet" href="itproject.css" type="text/css">
<title>IT Project: Project: Create New</title>
<style type="text/css">
<!--
.style37 {font-family: Arial, Helvetica, sans-serif; font-size: 14; }
.style38 {font-size: 14}
-->
</style>
</head>

<body>

<?php

$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}



/*

id
name
manager
start_date
end_date
actual_start_date
actual_end_date
status
privacy
description

*/

$sql = sprintf("SELECT name,start_date,end_date,status,description FROM projects WHERE id = %d", $id);

//echo $sql;


$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

$row = $result->fetch_assoc()


?>


<form name="form1" method="post" action="projectsDoEdit.php">

<?php

echo "<input type=\"hidden\" name=\"id\" value=\"";
echo $id;
echo "\">";

?>


<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td colspan="2"><p>&nbsp;</p>
      <table width="95%" border="0" align="center" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" class="loginsubtitlebarmain">Edit Projects</td>
        </tr>
        <tr class="maintext">
          <td width="21%">&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td>&nbsp;</td>
        </tr>
        <tr class="maintext">
          <td><table width="70%" border="1" align="center" cellpadding="1" cellspacing="0" bordercolor="#CCCCCC" class="dialogbox">
            <tr>
              <td width="50%" class="style37"><p class="style37">Project Name: *</p></td>
              <td width="50%"><span class="style38">

				
<?php

				
echo "<input type=\"text\" name=\"projectname\" value=\"";
echo $row['name'];
echo "\">";

?>
              </span></td>
            </tr>
            <tr>
              <td class="style37">Project Description:</td>
              <td><span class="style38">
                <textarea name="projectdescription" id="projectdescription" cols="45" rows="5" tabindex="2">
                

<?php

echo $row['description'];


?>                
                
                </textarea>

              </span></td>
            </tr>
            <tr>
              <td class="style37">Project Start Date: *</td>
              <td>

<?php

$start_date = $row['start_date'];
$end_date = $row['end_date'];

$sdatestamp = strtotime($start_date);
$edatestamp = strtotime($end_date);

$selmon = date('n',$sdatestamp);
$selday =  date('j',$sdatestamp);
$selyear = date('Y',$sdatestamp);

echo GenerateSelectMonths("smonth",$selmon);
echo GenerateSelectDays("sday", 1, 31, $selday);
echo GenerateSelectYears("syear", 2010, 2015, $selyear);

$eselmon = date('n',$edatestamp);
$eselday =  date('j',$edatestamp);
$eselyear = date('Y',$edatestamp);



?>              </td>
            </tr>
            <tr>
              <td class="style37">Project End Date: *</td>
              <td>

<?php


echo GenerateSelectMonths("emonth",$eselmon);
echo GenerateSelectDays("eday", 1, 31, $eselday);
echo GenerateSelectYears("eyear", 2010, 2015, $eselyear);


 
$conn->close();



?>              </td>
            </tr>
            
            <tr>
              <td class="style37"><span class="style38"></span></td>
              <td><span class="style38"></span></td>
            </tr>
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">Project Members: *</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            
            
<?php

$conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbase);
if ($conn->connect_error) {
    die('Could not connect: ' . $conn->error);
}

 
 
    die ('Can\'t use foo : ' . $conn->error);
}


/*

projects_members
project_id
username

get all existing members of this project

*/

$sql2 = sprintf("SELECT username FROM projects_members WHERE project_id = %d", $id);

$result = $conn->query($sql2);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql2;
    die($message);
}

while ( $row2 = mysql_fetch_array($result2, MYSQL_ASSOC) ) {
	$k = $row2['username'];
	$project_members[$k] = $k;
}


/*

username
firstname
lastname
email
position_id
group_id
password

*/

$sql = "SELECT username,firstname,lastname FROM users";

//echo $sql;


$result = $conn->query($sql);
if (!$result) {
    $message  = 'Invalid query: ' . $conn->error . "\n";
    $message .= 'Whole query: ' . $sql;
    die($message);
}

while ( $row = $result->fetch_assoc() ) {
	if ( $row['username'] != "administrator" )  {
		echo "<tr>\n";
		echo "<td colspan=\"2\">\n";
		
		if ( count($project_members) > 0 )  {
			if ( array_key_exists($row['username'], $project_members) )  {
				echo "<input type=\"checkbox\" name=\"projectmembers[]\" value=\"" . $row['username'] . "\" checked>\n";
			}  else  {
				echo "<input type=\"checkbox\" name=\"projectmembers[]\" value=\"" . $row['username'] . "\">\n";
			}
		}  else  {
			echo "<input type=\"checkbox\" name=\"projectmembers[]\" value=\"" . $row['username'] . "\">\n";
		}
		
		echo $row['firstname'] . " " . $row['lastname'];	
		echo "</td>\n";
		echo "</tr>\n";
	}
}

 
$conn->close();


?>



            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" class="style37">* required fields</td>
            </tr>
            <tr>
              <td colspan="2" class="style37"><label>
                  <div align="center">
                    <input name="createproject" type="submit" class="dialogboxbuttons" id="createproject" value="     SUBMIT MODIFICATIONS     " />
                  </div>
                </label></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <p>&nbsp;</p>
  </tr>
</table>
</form>
</body>
</html>
